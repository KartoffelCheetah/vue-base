# Vue Base

Different initialisation techniques for [Vue.js](https://vuejs.org/index.html) applications:

* [vanilla](https://gitlab.com/KartoffelCheetah/vue-base/-/tree/vanilla-script-tag)
* [esm](https://gitlab.com/KartoffelCheetah/vue-base/-/tree/esm-module)
* [webpack](https://gitlab.com/KartoffelCheetah/vue-base/-/tree/webpack)
